/*
 * This file is part of ARSnova Mobile.
 * Copyright (C) 2011-2012 Christian Thomas Weber
 * Copyright (C) 2012-2015 The ARSnova Team
 *
 * ARSnova Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARSnova Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARSnova Mobile.  If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('ARSnova.view.home.MotdPanel', {
	extend: 'Ext.Panel',

	requires: [
		'ARSnova.view.home.SessionList',
		'Ext.ux.Fileup',
		'ARSnova.view.home.SessionExportListPanel',
		'ARSnova.controller.SessionImport'
	],

	config: {
		fullscreen: true,
		scrollable: {
			direction: 'vertical',
			directionLock: true
		}
	},

	/* toolbar items */
	toolbar: null,
	backButton: null,

	/* items */
	createdSessions: null,

	initialize: function () {
		this.callParent(arguments);
		this.createToolbar();

		this.newMessagesButtonForm = Ext.create('Ext.Panel', {
			layout: {
				type: 'hbox',
				pack: 'center'
			},

			style: {
				marginTop: '30px'
			},

			items: [
				Ext.create('ARSnova.view.MatrixButton', {
					id: 'new-motd-button',
					text: Messages.CREATE_NEW_MOTD,
					buttonConfig: 'icon',
					cls: 'actionButton',
					imageCls: 'icon-info thm-green',
					scope: this,
					handler: function () {
						this.newMotdPanel = Ext.create('ARSnova.view.home.NewMotdPanel');
						var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
						hTP.animateActiveItem(this.newMotdPanel, 'slide');
					}
				})
			]
		});

		this.messagesForm = Ext.create('ARSnova.view.home.SessionList', {
			scrollable: null,
			title: Messages.MY_MESSAGES
		});


		this.matrixButtonPanel = Ext.create('Ext.Panel', {
			layout: {
				type: 'hbox',
				pack: 'center'
			}
		});

		this.on('activate', this.loadMyMessages);

		this.add([
			this.newMessagesButtonForm,
			this.messagesForm
		]);
	},

	createToolbar: function () {
		var screenWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		this.logoutButton = Ext.create('Ext.Button', {
			id: 'logout-button',
			text: Messages.LOGOUT,
			align: 'left',
			ui: 'back',
			hidden: true,
			handler: function () {
				ARSnova.app.getController('Auth').logout();
			}
		});

		this.backButton = Ext.create('Ext.Button', {
			text: Messages.SESSIONS,
			ui: 'back',
			handler: function () {
				var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
				hTP.animateActiveItem(hTP.mySessionsPanel, {
					type: 'slide',
					direction: 'right',
					duration: 700
				});
			}
		});

		this.toolbar = Ext.create('Ext.TitleBar', {
			title: Messages.MESSAGEOFTHEDAY,
			cls: 'speakerTitleText',
			docked: 'top',
			ui: 'light',
			items: [
				this.backButton,
				this.logoutButton
			]
		});
		this.add([this.toolbar]);
	},

	loadMyMessages: function () {
		if (ARSnova.app.userRole !== ARSnova.app.USER_ROLE_SPEAKER) {
			return;
		}
		var me = this;
		var promise = new RSVP.Promise();

		var hideLoadingMask = ARSnova.app.showLoadMask(Messages.LOAD_MASK_SEARCH);

		ARSnova.app.getController('Motds').getMotds({
			success: function (result) {
				me.displayMessages(result, me.messagesForm, hideLoadingMask);
			}
		});
		return promise;
	},

	displayMessages: function (messages, form, hideLoadingMask) {
		if (messages && messages.length !== 0) {
			form.removeAll();
			form.show();

			var buttonHandler = function (target) {
				var me = this;
				var hideLoadMask = ARSnova.app.showLoadMask(Messages.LOAD_MASK);
				this.newMotdPanel = Ext.create('ARSnova.view.home.NewMotdPanel', {
					message: target.config.messageObj,
					editMessage: true
				});
				var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
				hTP.animateActiveItem(me.newMotdPanel, {
					type: 'slide',
					direction: 'left',
					duration: 700
				});
				hideLoadMask();
			};

			for (var i = 0; i < messages.length; i++) {
				var message = messages[i];

				var icon = "icon-info thm-green";
				if (message.courseType && message.courseType.length > 0) {
					icon = "icon-info";
				}

				var iconCls = icon + " courseIcon";


				// Minimum width of 481px equals at least landscape view
				var messagekey = '<span class="messageButtonKeyword"> (' + message.message + ')</span>';
				var displaytext = window.innerWidth > 481 ?
					Ext.util.Format.htmlEncode(message.title) + messagekey :
					Ext.util.Format.htmlEncode(message.message);

				var messageButton = Ext.create('ARSnova.view.MultiBadgeButton', {
					xtype: 'button',
					ui: 'normal',
					text: displaytext,
					cls: 'forwardListButton',
					iconCls: iconCls,
					controller: 'messages',
					action: 'showDetails',
					badgeCls: 'badgeicon',
					messageObj: message,
					handler: buttonHandler
				});

				messageButton.setBadge([
					{badgeText: message.numInterposed, badgeCls: "feedbackQuestionsBadgeIcon"},
					{badgeText: message.numQuestions, badgeCls: "questionsBadgeIcon"},
					{badgeText: message.numAnswers, badgeCls: "answersBadgeIcon"}
				]);

				form.addEntry(messageButton);

				if (!message.active) {
					this.down('button[text=' + displaytext + ']').addCls("isInactive");
				}
			}
		} else {
			form.hide();
		}
		hideLoadingMask();
	}


});
