/*
 * This file is part of ARSnova Mobile.
 * Copyright (C) 2011-2012 Christian Thomas Weber
 * Copyright (C) 2012-2015 The ARSnova Team
 *
 * ARSnova Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARSnova Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARSnova Mobile.  If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('ARSnova.view.home.NewMotdPanel', {
	extend: 'Ext.Panel',

	config: {
		fullscreen: true,
		scrollable: true,
		scroll: 'vertical',
		editMessage: false,
		message: null
	},

	sessionKey: null,

	/* toolbar items */
	toolbar: null,
	backButton: null,

	/*Editor Panel*/
	markdownEditPanel: null,
	subject: null,
	textarea: null,
	saveAndContinueButton: null,
	startTimeDatePicker: null,
	endTimeDatePicker: null,
	targetAudience: null,

	/*Audience constants*/
	TARGET_EVERYONE: 2,
	TARGET_SPEAKER: 1,
	TARGET_STUDENT: 0,

	constructor: function (args) {
		this.callParent(arguments);
		var me = this;
		this.backButton = Ext.create('Ext.Button', {
			text: Messages.BACK,
			ui: 'back',
			handler: function () {
				var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
				hTP.animateActiveItem(hTP.motdPanel, {
					type: 'slide',
					direction: 'right',
					duration: 700
				});
			}
		});

		this.toolbar = Ext.create('Ext.Toolbar', {
			title: Messages.NEW_MOTD,
			cls: 'titlePaddingLeft',
			docked: 'top',
			ui: 'light',
			items: [
				this.backButton
			]
		});

		this.subject = Ext.create('Ext.field.Text', {
			name: 'motdSubject',
			placeHolder: Messages.CATEGORY_PLACEHOLDER
		});

		this.textarea = Ext.create('Ext.plugins.ResizableTextArea', {
			name: 'text',
			placeHolder: Messages.FORMAT_PLACEHOLDER
		});

		this.startTimeDatePicker = Ext.create('Ext.field.DatePicker', {
			name: 'startTimeDatePicker',
			label: 'Message Startdate',
			value: new Date()
		});

		this.endTimeDatePicker = Ext.create('Ext.field.DatePicker', {
			name: 'endTimeDatePicker',
			label: 'Message Enddate',
			value: new Date()
		});

		this.targetAudience = Ext.create('Ext.field.Select', {
			name: 'targetAudience',
			label: Messages.TARGET_AUDIENCE,
			options: [
				{text: Messages.TARGET_EVERYONE, value: this.TARGET_EVERYONE},
				{text: Messages.TARGET_SPEAKER, value: this.TARGET_SPEAKER},
				{text: Messages.TARGET_STUDENT, value: this.TARGET_STUDENT}
			]
		});

		this.deleteMessageButton = Ext.create('ARSnova.view.MatrixButton', {
			buttonConfig: 'icon',
			cls: 'actionButton',
			imageCls: 'icon-close thm-red',
			scope: this,
			hidden: true,
			handler: this.onDelete
		});


		this.saveAndContinueButton = Ext.create('Ext.Button', {
			ui: 'confirm',
			cls: 'saveQuestionButton',
			text: Messages.SAVE_NEW_MESSAGE,
			style: 'margin-top: 70px',
			scope: this,
			handler: this.onSubmit
		});

		this.add([this.toolbar, {
			title: 'createMessage',
			style: {
				marginTop: '15px'
			},
			xtype: 'formpanel',
			scrollable: null,
			id: 'createMessage',
			submitOnAction: false,

			items: [{
				xtype: 'fieldset',
				items: [
					this.subject,
					this.textarea,
					this.startTimeDatePicker,
					this.endTimeDatePicker,
					this.targetAudience,
					this.deleteMessageButton
				]
			}, this.saveAndContinueButton]
		}]);
		this.on('activate', this.onActivate);
	},

	onActivate: function () {
		if (this.config.editMessage) {
			this.subject.setValue(this.config.message.title);
			this.textarea.setValue(this.config.message.message);
			this.startTimeDatePicker.setValue(new Date(this.config.message.startTime));
			this.endTimeDatePicker.setValue(new Date(this.config.message.endTime));
			this.targetAudience.setValue(this.config.message.targetAudience);
			this.deleteMessageButton.setHidden(false);
		}
	},

	onSubmit: function () {
		var me = this;
		var subject = this.subject.getValue();
		var text = this.textarea.getValue();
		var startTimeValue = this.formatDate(this.startTimeDatePicker.getValue());
		var endTimeValue = this.formatDate(this.endTimeDatePicker.getValue());
		var target = this.targetAudience.getValue();

		if (this.config.editMessage) {
			ARSnova.app.getController('Motds').updateMotd({
				_id: me.config.message._id,
				title: subject,
				message: text,
				startTime: startTimeValue,
				endTime: endTimeValue,
				targetAudience: target
			});
		} else {
			ARSnova.app.getController('Motds').create({
				title: subject,
				message: text,
				startTime: startTimeValue,
				endTime: endTimeValue,
				targetAudience: target
			});
		}
		var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
		hTP.animateActiveItem(hTP.motdPanel, {
			type: 'slide',
			direction: 'right',
			duration: 500
		});
		Ext.Msg.alert(Messages.MESSAGEOFTHEDAY, Messages.SAVE_MOTD, Ext.emptyFn);
	},

	onDelete: function () {
		ARSnova.app.getController('Motds').deleteMotd(this.config.message._id);
		var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
		hTP.animateActiveItem(hTP.motdPanel, {
			type: 'slide',
			direction: 'right',
			duration: 500
		});
	},

	/**
	* http://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
	*/
	formatDate: function (date) {
		var month = '' + (date.getMonth() + 1);
		var day = '' + date.getDate();
		var year = date.getFullYear();

		if (month.length < 2) {
			month = '0' + month;
		}
		if (day.length < 2) {
			day = '0' + day;
		}

		return [year, month, day].join('-');
	}

});
