Ext.define('ARSnova.model.Motd', {
	extend: 'Ext.data.Model',

	mixin: ['Ext.mixin.Observable'],

	config: {
		idProperty: '_id',
		proxy: {type: 'restProxy'},
		fields: [
			'type',
			'title',
			'message',
			'startTime',
			'endTime',
			'targetAudience'
		]
	},

	constructor: function () {
		this.callParent(arguments);
	},

	create: function (callbacks) {
		return this.getProxy().createMotd(this, callbacks);
	},

	getMotds: function (callbacks) {
		return this.getProxy().readMotds(callbacks);
	},

	getMotdsByTarget: function (target, callbacks) {
		return this.getProxy().readMotdsByTarget(target, callbacks);
	},

	updateMotd: function (motd, callbacks) {
		return this.getProxy().updateMotd(motd, callbacks);
	},

	deleteMotd: function (id, callbacks) {
		return this.getProxy().deleteMotd(id, callbacks);
	}
});
