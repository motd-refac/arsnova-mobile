Ext.define("ARSnova.controller.Motds", {
	extend: 'Ext.app.Controller',

	create: function (options) {
		var motd = Ext.create('ARSnova.model.Motd', {
			type: 'motd',
			title: options.title,
			message: options.message,
			startTime: options.startTime,
			endTime: options.endTime,
			targetAudience: options.targetAudience
		});
		motd.create({
			success: function () {
				Ext.Msg.alert(Messages.MESSAGEOFTHEDAY, Messages.SAVE_NEW_MESSAGE, Ext.emptyFn);
			}
		});
	},

	getMotds: function (callbacks) {
		ARSnova.app.motdModel.getMotds({
			success: function (result) {
				callbacks.success(result);
			}
		});
	},

	getMotdsByTarget: function (target, callbacks) {
		ARSnova.app.motdModel.getMotdsByTarget(target, {
			success: function (result) {
				callbacks.success(result);
			}
		});
	},

	updateMotd: function (options) {
		ARSnova.app.motdModel.updateMotd(options, {
			success: function (result) {
				console.log("updated motd");
			}
		});
	},

	deleteMotd: function (id) {
		ARSnova.app.motdModel.deleteMotd(id, {
			success: function (result) {
				console.log("deleted motd");
			}
		});
	}
});
